$(function () {
    let minTime = $('.js-calendar').data('min-time');
    let slotDuration = $('.js-calendar').data('slot-duration');
    let interval = $('.js-calendar').data('interval');
    let restaurant = $('.js-calendar').data('restaurant');

    $("#calendar-holder").fullCalendar({
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        viewRender: function(view) {
        //note: this is a hack, i don't know why the view title keep showing "undefined" text in it.
        //probably bugs in jquery fullcalendar
        $('.fc-left')[0].children[0].innerText = view.title.replace(new RegExp("undefined", 'g'), ""); ;

        },
        defaultView: 'timelineDay',
        height: 0.8*window.innerHeight,
        nowIndicator: true,
        minTime: minTime,
        slotDuration: slotDuration,
        slotLabelInterval: {
            minutes: interval
        },
        slotLabelFormat: [
            'H:00',
            'mm'
        ],
        resourceLabelText: 'Tables',
        resourceAreaWidth: "12%",
        resources: {
            url: '/fc-load-resources',
            type: 'POST',
            data: {
                filters: {
                    restaurant: restaurant
                }
            },
            error: function () {
                alert("There was an error while fetching FullCalendar!");
            }
        },
        events: {
            url: '/fc-load-events',
            type: 'POST',
            data: {
                filters: {
                    restaurant: restaurant
                }
            },
            error: function () {
                alert("There was an error while fetching FullCalendar!");
            }
        },
        eventColor: '#0c5580',
        eventBorderColor: '#000000'
    });
});
<?php

namespace UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use ReservationBundle\Entity\ReservationRequest;
use ReservationBundle\Entity\Restaurant;

/**
 * @ORM\Entity
 * @ORM\Table(name="`user`")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="first_name", type="string", nullable=false)
     */
    private $firstName;

    /**
     * @ORM\Column(name="surname", type="string", nullable=false)
     */
    private $surname;

    /**
     * @ORM\Column(name="phoneNumber", type="string", nullable=false)
     */
    private $phoneNumber;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="ReservationBundle\Entity\ReservationRequest", mappedBy="user")
     */
    protected $reservations;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="ReservationBundle\Entity\Restaurant", mappedBy="manager")
     */
    protected $restaurants;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->reservations = new ArrayCollection();
        $this->restaurants = new ArrayCollection();
    }

    /**
     * Overrides the parent function to set username same with email
     * because username in FOSUserBundle is mandatory.
     * 
     * @param string $email
     * 
     * @return $this|static
     */
    public function setEmail($email)
    {
        $this->setUsername($email);

        return parent::setEmail($email);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * Get Reservations for this user
     *
     * @return ArrayCollection
     */
    public function getReservations()
    {
        return $this->reservations;
    }

    /**
     * Adds a reservation to the reservation list of this User
     *
     * @param ReservationRequest $reservation
     *
     * @return User
     */
    public function addReservation(ReservationRequest $reservation)
    {
        $this->reservations[] = $reservation;

        return $this;
    }

    /**
     * Remove review.
     *
     * @param ReservationRequest $reservation
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeReview(ReservationRequest $reservation)
    {
        return $this->reservations->removeElement($reservation);
    }

    /**
     * Get Restaurants for this user
     *
     * @return ArrayCollection
     */
    public function getRestaurants()
    {
        return $this->restaurants;
    }

    /**
     * Adds a restaurant to the restaurant list of this User
     *
     * @param Restaurant $restaurant
     *
     * @return User
     */
    public function addRestaurant(Restaurant $restaurant)
    {
        $this->restaurants[] = $restaurant;

        return $this;
    }

    /**
     * Remove restaurant.
     *
     * @param Restaurant $restaurant
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRestaurant(Restaurant $restaurant)
    {
        return $this->restaurants->removeElement($restaurant);
    }
}

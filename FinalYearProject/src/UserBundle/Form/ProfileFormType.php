<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\ProfileFormType as BaseProfileFormType;


class ProfileFormType extends AbstractType
{

    /**
     * builds the profile form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('surname')
            ->remove('username');
    }

    /**
     * Extending the parent profile form to add/remove more fields
     *
     * @return mixed
     */
    public function getParent()
    {
        return BaseProfileFormType::class;
    }
}
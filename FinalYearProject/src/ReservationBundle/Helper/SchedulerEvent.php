<?php

namespace ReservationBundle\Helper;

use DateInterval;
use DateTime;
use http\Exception\InvalidArgumentException;
use Toiba\FullCalendarBundle\Entity\Event;

/**
 * Class SchedulerEvent
 *
 * @package ReservationBundle\Helper
 */
class SchedulerEvent extends Event
{
    /** @var array */
    protected $resourceIds = [];


    /**
     * SchedulerEvent constructor.
     *
     * @param string $title
     * @param int $dinnerDuration
     * @param DateTime $start
     * @param array $resourceIds
     */
    public function __construct(
        $title,
        $dinnerDuration,
        DateTime $start,
        array $resourceIds = []
    ) {
        try {
            $interval = new DateInterval('PT' . $dinnerDuration . 'M');
        } catch (\Exception $exception) {
            throw new InvalidArgumentException($exception);
        }

        $end = clone $start;
        $end->add($interval);

        parent::__construct($title, $start, $end);
        $this->resourceIds = $resourceIds;
    }


    /**
     * @return array
     */
    public function getResourceIds()
    {
        return $this->resourceIds;
    }

    /**
     * @param array $resourceIds
     */
    public function setResourceIds($resourceIds)
    {
        $this->resourceIds = $resourceIds;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $event = parent::toArray();
        $event['resourceIds'] = $this->getResourceIds();

        return $event;
    }
}

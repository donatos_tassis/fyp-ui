<?php

namespace ReservationBundle\Helper;

use Doctrine\Common\Persistence\ObjectManager;
use ReservationBundle\Entity\AtomicTable;
use ReservationBundle\Entity\CompoundTable;
use ReservationBundle\Entity\ReservationRequest;
use ReservationBundle\Entity\Restaurant;

/**
 * Class RequestOption
 *
 * @package ReservationBundle\Helper
 */
class RequestOptions
{
    /** @var ObjectManager */
    private $em;

    /** @var array */
    private $tables = [];

    /** @var array */
    private $parties = [];

    /** @var array */
    private $compoundTables = [];

    /** @var Restaurant */
    private $restaurant;

    /**
     * The day in format 'Y-m-d'
     *
     * @var string
     */
    private $day;

    /**
     * RequestOptions constructor.
     *
     * @param ObjectManager $em
     * @param Restaurant $restaurant
     * @param string $day
     */
    public function __construct(ObjectManager $em, Restaurant $restaurant, $day)
    {
        $this->em = $em;
        $this->restaurant = $restaurant;
        $this->day = $day;
        $this->setTables();
        $this->setParties();
        $this->setCompoundTables();
    }

    /**
     * Sets the array of tables
     */
    private function setTables()
    {
        $tables = $this->em->getRepository(AtomicTable::class)->getTablesPerRestaurant($this->restaurant);

        /** @var AtomicTable $table */
        foreach ($tables as $table) {
            array_push($this->tables, [
                'id' => $table->getId(),
                'capacity' => $table->getCapacity()
            ]);
        }
    }

    /**
     * Sets the array of parties
     */
    private function setParties()
    {
        $parties = $this->em->getRepository(ReservationRequest::class)
            ->getAllReservationsPerDayAndRestaurant($this->restaurant->getId(), $this->day);

        /** @var ReservationRequest $party */
        foreach ($parties as $party) {
            $ctTableId = 0;

            if ($party->getCompoundTable() !== null)
                $ctTableId = $party->getCompoundTable()->getId();

            array_push($this->parties, [
                'id' => $party->getId(),
                'size' => $party->getPartySize(),
                'start' => $party->getTimeSlotReservedFor(),
                'compoundTable' => $ctTableId
            ]);
        }
    }

    /**
     * Sets the array of compoundTables
     */
    private function setCompoundTables()
    {
        $compoundTables = $this->em->getRepository(CompoundTable::class)
            ->getCompoundTablesPerRestaurant($this->restaurant);

        /** @var CompoundTable $compoundTable */
        foreach ($compoundTables as $compoundTable) {
            array_push($this->compoundTables, [
                'id' => $compoundTable->getId(),
                'tables' => $compoundTable->getTablesToArray(),
                'capacity' => $compoundTable->getCapacity()
            ]);
        }
    }

    /**
     * Return request options in array format
     *
     * @return array
     * @throws \Exception
     */
    public function toArray()
    {
        return [
            'json' => [
                'restaurant' => $this->restaurant->getId(),
                'date' => $this->day,
                'timeSlots' => $this->restaurant->getTotalTimeSlots(),
                'targetCovers' => $this->restaurant->getTargetCovers(),
                'dinnerDuration' => $this->restaurant->getMinDinnerDuration() / $this->restaurant->getTimeInterval(),
                'failedRequests' => $this->em->getRepository(ReservationRequest::class)
                    ->getRejectedReservationsPerDayAndRestaurant($this->restaurant, $this->day),
                'tables' => $this->tables,
                'parties' => $this->parties,
                'compoundTables' => $this->compoundTables
            ]
        ];
    }
}

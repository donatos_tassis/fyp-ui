<?php

namespace ReservationBundle\Helper;

/**
 * Class DataProvider
 * @package ReservationBundle\Helper
 */
class DataProvider
{
    /** @const array */
    const RESTAURANTS_DATA = [
        [
            'prefix' => 'A',
            'totalTables' => 5,
            'compoundTables' => [
                [
                    'atomicTables' => [1, 2]
                ],
                [
                    'atomicTables' => [1, 2, 3]
                ],
                [
                    'atomicTables' => [2, 3]
                ],
                [
                    'atomicTables' => [4, 5]
                ],
            ]
        ],
        [
            'prefix' => 'B',
            'totalTables' => 15,
            'compoundTables' => [
                [
                    'atomicTables' => [1, 2]
                ],
                [
                    'atomicTables' => [1, 2, 3]
                ],
                [
                    'atomicTables' => [2, 3]
                ],
                [
                    'atomicTables' => [4, 5]
                ],
                [
                    'atomicTables' => [4, 5, 6]
                ],
                [
                    'atomicTables' => [4, 5, 6, 7]
                ],
                [
                    'atomicTables' => [5, 6]
                ],
                [
                    'atomicTables' => [5, 6, 7]
                ],
                [
                    'atomicTables' => [6, 7]
                ],
                [
                    'atomicTables' => [11, 12]
                ],
                [
                    'atomicTables' => [11, 12, 13]
                ],
                [
                    'atomicTables' => [12, 13]
                ],
                [
                    'atomicTables' => [14, 15]
                ],
            ]
        ],
        [
            'prefix' => 'C',
            'totalTables' => 30,
            'compoundTables' => [
                [
                    'atomicTables' => [1, 2]
                ],
                [
                    'atomicTables' => [1, 2, 3]
                ],
                [
                    'atomicTables' => [2, 3]
                ],
                [
                    'atomicTables' => [4, 5]
                ],
                [
                    'atomicTables' => [4, 5, 6]
                ],
                [
                    'atomicTables' => [4, 5, 6, 7]
                ],
                [
                    'atomicTables' => [5, 6]
                ],
                [
                    'atomicTables' => [5, 6, 7]
                ],
                [
                    'atomicTables' => [6, 7]
                ],
                [
                    'atomicTables' => [11, 12]
                ],
                [
                    'atomicTables' => [11, 12, 13]
                ],
                [
                    'atomicTables' => [12, 13]
                ],
                [
                    'atomicTables' => [14, 15]
                ],
                [
                    'atomicTables' => [14, 15, 16]
                ],
                [
                    'atomicTables' => [14, 15, 16, 17]
                ],
                [
                    'atomicTables' => [15, 16]
                ],
                [
                    'atomicTables' => [15, 16, 17]
                ],
                [
                    'atomicTables' => [16, 17]
                ],
                [
                    'atomicTables' => [21, 22]
                ],
                [
                    'atomicTables' => [21, 22, 23]
                ],
                [
                    'atomicTables' => [22, 23]
                ],
                [
                    'atomicTables' => [24, 25]
                ],
                [
                    'atomicTables' => [24, 25, 26]
                ],
                [
                    'atomicTables' => [24, 25, 26, 27]
                ],
                [
                    'atomicTables' => [25, 26]
                ],
                [
                    'atomicTables' => [25, 26, 27]
                ],
                [
                    'atomicTables' => [26, 27]
                ]
            ]
        ],
        [
            'prefix' => 'D',
            'totalTables' => 50,
            'compoundTables' => [
                [
                    'atomicTables' => [1, 2]
                ],
                [
                    'atomicTables' => [1, 2, 3]
                ],
                [
                    'atomicTables' => [2, 3]
                ],
                [
                    'atomicTables' => [4, 5]
                ],
                [
                    'atomicTables' => [4, 5, 6]
                ],
                [
                    'atomicTables' => [4, 5, 6, 7]
                ],
                [
                    'atomicTables' => [5, 6]
                ],
                [
                    'atomicTables' => [5, 6, 7]
                ],
                [
                    'atomicTables' => [6, 7]
                ],
                [
                    'atomicTables' => [11, 12]
                ],
                [
                    'atomicTables' => [11, 12, 13]
                ],
                [
                    'atomicTables' => [12, 13]
                ],
                [
                    'atomicTables' => [14, 15]
                ],
                [
                    'atomicTables' => [14, 15, 16]
                ],
                [
                    'atomicTables' => [14, 15, 16, 17]
                ],
                [
                    'atomicTables' => [15, 16]
                ],
                [
                    'atomicTables' => [15, 16, 17]
                ],
                [
                    'atomicTables' => [16, 17]
                ],
                [
                    'atomicTables' => [21, 22]
                ],
                [
                    'atomicTables' => [21, 22, 23]
                ],
                [
                    'atomicTables' => [22, 23]
                ],
                [
                    'atomicTables' => [24, 25]
                ],
                [
                    'atomicTables' => [24, 25, 26]
                ],
                [
                    'atomicTables' => [24, 25, 26, 27]
                ],
                [
                    'atomicTables' => [25, 26]
                ],
                [
                    'atomicTables' => [25, 26, 27]
                ],
                [
                    'atomicTables' => [26, 27]
                ],
                [
                    'atomicTables' => [31, 32]
                ],
                [
                    'atomicTables' => [31, 32, 33]
                ],
                [
                    'atomicTables' => [32, 33]
                ],
                [
                    'atomicTables' => [34, 35]
                ],
                [
                    'atomicTables' => [34, 35, 36]
                ],
                [
                    'atomicTables' => [34, 35, 36, 37]
                ],
                [
                    'atomicTables' => [35, 36]
                ],
                [
                    'atomicTables' => [35, 36, 37]
                ],
                [
                    'atomicTables' => [36, 37]
                ],
                [
                    'atomicTables' => [41, 42]
                ],
                [
                    'atomicTables' => [41, 42, 43]
                ],
                [
                    'atomicTables' => [42, 43]
                ],
                [
                    'atomicTables' => [44, 45]
                ],
                [
                    'atomicTables' => [44, 45, 46]
                ],
                [
                    'atomicTables' => [44, 45, 46, 47]
                ],
                [
                    'atomicTables' => [45, 46]
                ],
                [
                    'atomicTables' => [45, 46, 47]
                ],
                [
                    'atomicTables' => [46, 47]
                ],
            ]
        ],
        [
            'prefix' => 'E',
            'totalTables' => 80,
            'compoundTables' => [
                [
                    'atomicTables' => [1, 2]
                ],
                [
                    'atomicTables' => [1, 2, 3]
                ],
                [
                    'atomicTables' => [2, 3]
                ],
                [
                    'atomicTables' => [4, 5]
                ],
                [
                    'atomicTables' => [4, 5, 6]
                ],
                [
                    'atomicTables' => [4, 5, 6, 7]
                ],
                [
                    'atomicTables' => [5, 6]
                ],
                [
                    'atomicTables' => [5, 6, 7]
                ],
                [
                    'atomicTables' => [6, 7]
                ],
                [
                    'atomicTables' => [11, 12]
                ],
                [
                    'atomicTables' => [11, 12, 13]
                ],
                [
                    'atomicTables' => [12, 13]
                ],
                [
                    'atomicTables' => [14, 15]
                ],
                [
                    'atomicTables' => [14, 15, 16]
                ],
                [
                    'atomicTables' => [14, 15, 16, 17]
                ],
                [
                    'atomicTables' => [15, 16]
                ],
                [
                    'atomicTables' => [15, 16, 17]
                ],
                [
                    'atomicTables' => [16, 17]
                ],
                [
                    'atomicTables' => [21, 22]
                ],
                [
                    'atomicTables' => [21, 22, 23]
                ],
                [
                    'atomicTables' => [22, 23]
                ],
                [
                    'atomicTables' => [24, 25]
                ],
                [
                    'atomicTables' => [24, 25, 26]
                ],
                [
                    'atomicTables' => [24, 25, 26, 27]
                ],
                [
                    'atomicTables' => [25, 26]
                ],
                [
                    'atomicTables' => [25, 26, 27]
                ],
                [
                    'atomicTables' => [26, 27]
                ],
                [
                    'atomicTables' => [31, 32]
                ],
                [
                    'atomicTables' => [31, 32, 33]
                ],
                [
                    'atomicTables' => [32, 33]
                ],
                [
                    'atomicTables' => [34, 35]
                ],
                [
                    'atomicTables' => [34, 35, 36]
                ],
                [
                    'atomicTables' => [34, 35, 36, 37]
                ],
                [
                    'atomicTables' => [35, 36]
                ],
                [
                    'atomicTables' => [35, 36, 37]
                ],
                [
                    'atomicTables' => [36, 37]
                ],
                [
                    'atomicTables' => [41, 42]
                ],
                [
                    'atomicTables' => [41, 42, 43]
                ],
                [
                    'atomicTables' => [42, 43]
                ],
                [
                    'atomicTables' => [44, 45]
                ],
                [
                    'atomicTables' => [44, 45, 46]
                ],
                [
                    'atomicTables' => [44, 45, 46, 47]
                ],
                [
                    'atomicTables' => [45, 46]
                ],
                [
                    'atomicTables' => [45, 46, 47]
                ],
                [
                    'atomicTables' => [46, 47]
                ],
                [
                    'atomicTables' => [51, 52]
                ],
                [
                    'atomicTables' => [51, 52, 53]
                ],
                [
                    'atomicTables' => [52, 53]
                ],
                [
                    'atomicTables' => [54, 55]
                ],
                [
                    'atomicTables' => [54, 55, 56]
                ],
                [
                    'atomicTables' => [54, 55, 56, 57]
                ],
                [
                    'atomicTables' => [55, 56]
                ],
                [
                    'atomicTables' => [55, 56, 57]
                ],
                [
                    'atomicTables' => [56, 57]
                ],
                [
                    'atomicTables' => [61, 62]
                ],
                [
                    'atomicTables' => [61, 62, 63]
                ],
                [
                    'atomicTables' => [62, 63]
                ],
                [
                    'atomicTables' => [64, 65]
                ],
                [
                    'atomicTables' => [64, 65, 66]
                ],
                [
                    'atomicTables' => [64, 65, 66, 67]
                ],
                [
                    'atomicTables' => [65, 66]
                ],
                [
                    'atomicTables' => [65, 66, 67]
                ],
                [
                    'atomicTables' => [66, 67]
                ],
                [
                    'atomicTables' => [71, 72]
                ],
                [
                    'atomicTables' => [71, 72, 73]
                ],
                [
                    'atomicTables' => [72, 73]
                ],
                [
                    'atomicTables' => [74, 75]
                ],
                [
                    'atomicTables' => [74, 75, 76]
                ],
                [
                    'atomicTables' => [74, 75, 76, 77]
                ],
                [
                    'atomicTables' => [75, 76]
                ],
                [
                    'atomicTables' => [75, 76, 77]
                ],
            ]
        ],
        [
            'prefix' => 'F',
            'totalTables' => 100,
            'compoundTables' => [
                [
                    'atomicTables' => [1, 2]
                ],
                [
                    'atomicTables' => [1, 2, 3]
                ],
                [
                    'atomicTables' => [2, 3]
                ],
                [
                    'atomicTables' => [4, 5]
                ],
                [
                    'atomicTables' => [4, 5, 6]
                ],
                [
                    'atomicTables' => [4, 5, 6, 7]
                ],
                [
                    'atomicTables' => [5, 6]
                ],
                [
                    'atomicTables' => [5, 6, 7]
                ],
                [
                    'atomicTables' => [6, 7]
                ],
                [
                    'atomicTables' => [11, 12]
                ],
                [
                    'atomicTables' => [11, 12, 13]
                ],
                [
                    'atomicTables' => [12, 13]
                ],
                [
                    'atomicTables' => [14, 15]
                ],
                [
                    'atomicTables' => [14, 15, 16]
                ],
                [
                    'atomicTables' => [14, 15, 16, 17]
                ],
                [
                    'atomicTables' => [15, 16]
                ],
                [
                    'atomicTables' => [15, 16, 17]
                ],
                [
                    'atomicTables' => [16, 17]
                ],
                [
                    'atomicTables' => [21, 22]
                ],
                [
                    'atomicTables' => [21, 22, 23]
                ],
                [
                    'atomicTables' => [22, 23]
                ],
                [
                    'atomicTables' => [24, 25]
                ],
                [
                    'atomicTables' => [24, 25, 26]
                ],
                [
                    'atomicTables' => [24, 25, 26, 27]
                ],
                [
                    'atomicTables' => [25, 26]
                ],
                [
                    'atomicTables' => [25, 26, 27]
                ],
                [
                    'atomicTables' => [26, 27]
                ],
                [
                    'atomicTables' => [31, 32]
                ],
                [
                    'atomicTables' => [31, 32, 33]
                ],
                [
                    'atomicTables' => [32, 33]
                ],
                [
                    'atomicTables' => [34, 35]
                ],
                [
                    'atomicTables' => [34, 35, 36]
                ],
                [
                    'atomicTables' => [34, 35, 36, 37]
                ],
                [
                    'atomicTables' => [35, 36]
                ],
                [
                    'atomicTables' => [35, 36, 37]
                ],
                [
                    'atomicTables' => [36, 37]
                ],
                [
                    'atomicTables' => [41, 42]
                ],
                [
                    'atomicTables' => [41, 42, 43]
                ],
                [
                    'atomicTables' => [42, 43]
                ],
                [
                    'atomicTables' => [44, 45]
                ],
                [
                    'atomicTables' => [44, 45, 46]
                ],
                [
                    'atomicTables' => [44, 45, 46, 47]
                ],
                [
                    'atomicTables' => [45, 46]
                ],
                [
                    'atomicTables' => [45, 46, 47]
                ],
                [
                    'atomicTables' => [46, 47]
                ],
                [
                    'atomicTables' => [51, 52]
                ],
                [
                    'atomicTables' => [51, 52, 53]
                ],
                [
                    'atomicTables' => [52, 53]
                ],
                [
                    'atomicTables' => [54, 55]
                ],
                [
                    'atomicTables' => [54, 55, 56]
                ],
                [
                    'atomicTables' => [54, 55, 56, 57]
                ],
                [
                    'atomicTables' => [55, 56]
                ],
                [
                    'atomicTables' => [55, 56, 57]
                ],
                [
                    'atomicTables' => [56, 57]
                ],
                [
                    'atomicTables' => [61, 62]
                ],
                [
                    'atomicTables' => [61, 62, 63]
                ],
                [
                    'atomicTables' => [62, 63]
                ],
                [
                    'atomicTables' => [64, 65]
                ],
                [
                    'atomicTables' => [64, 65, 66]
                ],
                [
                    'atomicTables' => [64, 65, 66, 67]
                ],
                [
                    'atomicTables' => [65, 66]
                ],
                [
                    'atomicTables' => [65, 66, 67]
                ],
                [
                    'atomicTables' => [66, 67]
                ],
                [
                    'atomicTables' => [71, 72]
                ],
                [
                    'atomicTables' => [71, 72, 73]
                ],
                [
                    'atomicTables' => [72, 73]
                ],
                [
                    'atomicTables' => [74, 75]
                ],
                [
                    'atomicTables' => [74, 75, 76]
                ],
                [
                    'atomicTables' => [74, 75, 76, 77]
                ],
                [
                    'atomicTables' => [75, 76]
                ],
                [
                    'atomicTables' => [75, 76, 77]
                ],
                [
                    'atomicTables' => [76, 77]
                ],
                [
                    'atomicTables' => [81, 82]
                ],
                [
                    'atomicTables' => [81, 82, 83]
                ],
                [
                    'atomicTables' => [82, 83]
                ],
                [
                    'atomicTables' => [84, 85]
                ],
                [
                    'atomicTables' => [84, 85, 86]
                ],
                [
                    'atomicTables' => [84, 85, 86, 87]
                ],
                [
                    'atomicTables' => [85, 86]
                ],
                [
                    'atomicTables' => [85, 86, 87]
                ],
                [
                    'atomicTables' => [86, 87]
                ],
                [
                    'atomicTables' => [91, 92]
                ],
                [
                    'atomicTables' => [91, 92, 93]
                ],
                [
                    'atomicTables' => [92, 93]
                ],
                [
                    'atomicTables' => [94, 95]
                ],
                [
                    'atomicTables' => [94, 95, 96]
                ],
                [
                    'atomicTables' => [94, 95, 96, 97]
                ],
                [
                    'atomicTables' => [95, 96]
                ],
                [
                    'atomicTables' => [95, 96, 97]
                ],
                [
                    'atomicTables' => [96, 97]
                ],
            ]
        ]
    ];
}

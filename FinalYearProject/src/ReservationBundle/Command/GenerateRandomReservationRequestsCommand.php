<?php

namespace ReservationBundle\Command;

use DateTime;
use Doctrine\DBAL\DBALException;
use ReservationBundle\Entity\ReservationRequest;
use ReservationBundle\Entity\Restaurant;
use ReservationBundle\Helper\RequestOptions;
use ReservationBundle\Service\SearchApiRequest;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateRandomReservationRequestsCommand extends ContainerAwareCommand
{
    /** @const string */
    const COMMAND_DESCR = 'Generate random reservation requests';

    /** @const string */
    const COMMAND_HELP = 'This command generates random reservation requests for the given restaurant';

    /** @const string */
    const SEARCH_API_DOMAIN = 'search_api_domain';

    /** @const string */
    const URI_SIMPLE = 'search-simple';

    /** @const string */
    const URI_STRICT = 'search-strict';

    /** @const string */
    const URI_HYBRID = 'search-hybrid';


    protected function configure()
    {
        $this
            ->setName('reservation:generate:random')
            ->setDescription(self::COMMAND_DESCR)
            ->setHelp(self::COMMAND_HELP)
            ->addArgument(
                'restaurant',
                InputArgument::REQUIRED,
                'The restaurant id')
            ->addOption(
                'requests',
                'req',
                InputOption::VALUE_OPTIONAL,
                'The number of requests to create')
            ->addOption(
                'uri',
                null,
                InputOption::VALUE_OPTIONAL,
                'The uri that the requests are gonna be send for processing'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $restaurantId = $input->getArgument('restaurant');
        /** @var Restaurant $restaurant */
        $restaurant = $em->getRepository(Restaurant::class)->find($restaurantId);
        if(!$restaurant)
        {
            $output->writeln('ERROR: The 1st argument is not a valid restaurant id');
            $output->writeln('Please try again with a valid ID ...');
            return;
        }

        if($input->getOption('requests')) {
            $output->writeln('Generating random reservation requests...');
            $this->truncateReservationRequestTable();
            $numberOfRequests = $input->getOption('requests');
            $em->getRepository(ReservationRequest::class)
                ->generateRandomReservationRequests($restaurant, $numberOfRequests);
        }

        if($input->getOption('uri')) {
            $uri = $this->uriSelector($input, $output);
            if ($uri == null)
                return;

            $output->writeln('Waiting for response from search API...');
            $today = new DateTime('now');
            $day = $today->format('Y-m-d');
            $optionsService = new RequestOptions($em, $restaurant, $day);
            $options = $optionsService->toArray();

            $searchRequest = new SearchApiRequest();
            $url = $this->getContainer()->getParameter(self::SEARCH_API_DOMAIN) . $uri;
            $results = $searchRequest->request($url, $options);

            if(!array_key_exists('error', $results))
            $em->getRepository(ReservationRequest::class)
                ->updateReservationRequests($results['parties']);
        }

        $output->writeln('DONE');
    }

    /**
     * truncate table ReservationRequest
     * @throws DBALException
     */
    protected function truncateReservationRequestTable()
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $connection = $em->getConnection();
        $platform = $connection->getDatabasePlatform();
        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');
        $truncate = $platform->getTruncateTableSQL('reservation_request', true);
        $connection->executeUpdate($truncate);
        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 1;');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return string|null
     */
    protected function uriSelector(InputInterface $input, OutputInterface $output)
    {
        switch ($input->getOption('uri')) {
            case 1:
                $uri = self::URI_SIMPLE;
                break;
            case 2:
                $uri = self::URI_STRICT;
                break;
            case 3:
                $uri = self::URI_HYBRID;
                break;
            default:
                $output->writeln('ERROR: not valid option `uri`');
                $output->writeln('Please choose a value between 1 and 3.');
                return;
        }

        return $uri;
    }
}

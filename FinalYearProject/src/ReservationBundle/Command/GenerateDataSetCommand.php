<?php

namespace ReservationBundle\Command;

use Doctrine\DBAL\DBALException;
use ReservationBundle\Entity\AtomicTable;
use ReservationBundle\Entity\CompoundTable;
use ReservationBundle\Entity\Restaurant;
use ReservationBundle\Helper\DataProvider;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateDataSetCommand
 *
 * @package ReservationBundle\Command
 */
class GenerateDataSetCommand extends ContainerAwareCommand
{
    /** @const string */
    const COMMAND_DESCR = 'Generate test dataSet';

    /** @const string */
    const COMMAND_HELP = 'This command generates a test dataSet with '.
        'Restaurants - AtomicTables and CompoundTables into the database';


    protected function configure()
    {
        $this
            ->setName('reservation:generate:test-data')
            ->setDescription(self::COMMAND_DESCR)
            ->setHelp(self::COMMAND_HELP);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws DBALException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Generating test data-set...');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->truncateTables();
        $restaurants = $em->getRepository(Restaurant::class)->createRestaurants();

        foreach($restaurants as $key => $restaurant) {
            $atomicTables = $em->getRepository(AtomicTable::class)->createAtomicTables(
                $restaurant,
                DataProvider::RESTAURANTS_DATA[$key]['prefix'],
                DataProvider::RESTAURANTS_DATA[$key]['totalTables']
            );

            // create compound tables that are actually atomic tables (same as atomic tables)
            $em->getRepository(CompoundTable::class)->createSingleTableCompoundTables($atomicTables);
            $compoundTables = DataProvider::RESTAURANTS_DATA[$key]['compoundTables'];

            foreach($compoundTables as $compoundTable) {
                $tablesPerCompoundTable = [];
                $capacity = 0;

                foreach($compoundTable['atomicTables'] as $tableIndex) {
                    /** @var AtomicTable $atomicTable */
                    $atomicTable = $atomicTables[$tableIndex - 1];
                    $capacity += $atomicTable->getCapacity();
                    if($atomicTable->getCapacity() < 4)
                        $capacity++;
                    array_push($tablesPerCompoundTable, $atomicTable);
                }

                // create compound tables with 2 or more tables (supports up to 4 tables)
                $em->getRepository(CompoundTable::class)->createCompoundTable(
                    $restaurant,
                    $capacity - count($tablesPerCompoundTable),
                    $tablesPerCompoundTable
                );
            }
        }

        $output->writeln('DONE !!!');
    }

    /**
     * truncate table Restaurant
     * @throws DBALException
     */
    protected function truncateTables()
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $connection = $em->getConnection();
        $platform = $connection->getDatabasePlatform();
        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 0;');
        $truncatableTables = [
            'restaurant',
            'atomic_table',
            'compound_table'
        ];

        foreach ($truncatableTables as $table) {
            $truncate = $platform->getTruncateTableSQL($table, true);
            $connection->executeUpdate($truncate);
        }

        $connection->executeQuery('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
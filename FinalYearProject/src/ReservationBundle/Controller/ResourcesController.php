<?php

namespace ReservationBundle\Controller;

use ReservationBundle\Entity\AtomicTable;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ResourcesController
 *
 * @package ReservationBundle\Controller
 */
class ResourcesController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function loadAction(Request $request)
    {
        $filters = $request->get('filters', []);
        $restaurantId = $filters['restaurant'];
        $em = $this->getDoctrine()->getManager();
        $tables = $em->getRepository(AtomicTable::class)->getTablesPerRestaurant($restaurantId);

        $tablesToArray = [];
        /** @var AtomicTable $table */
        foreach ($tables as $field => $table) {
            $tablesToArray[$field] = [
                'id' => $table->getId(),
                'title' => $table->getName(). ' ('. $table->getCapacity(). ')'
            ];
        }

        try {
            $content = json_encode($tablesToArray);
            $status = empty($content) ? Response::HTTP_NO_CONTENT : Response::HTTP_OK;
        } catch (\Exception $exception) {
            $content = json_encode(array('error' => $exception->getMessage()));
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($content);
        $response->setStatusCode($status);

        return $response;
    }
}

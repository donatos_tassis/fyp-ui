<?php

namespace ReservationBundle\Controller;

use AppBundle\Exceptions\RestaurantNotFoundException;
use DateInterval;
use DateTime;
use ReservationBundle\Entity\Restaurant;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ManagementController extends Controller
{
    /**
     * @param int $id
     *
     * @return Response
     * @throws RestaurantNotFoundException
     * @throws \Exception
     */
    public function viewTimelineAction($id)
    {
        $user = $this->getUser();
        if(!$user)
            return $this->redirectToRoute('fos_user_security_login');

        $em = $this->getDoctrine()->getManager();
        $restaurant = $em->getRepository(Restaurant::class)->find($id);
        if(!$restaurant)
            throw new RestaurantNotFoundException();

        $roles = $user->getRoles();
        if($restaurant->getManager() != $user && !in_array('ROLE_ADMIN', $roles))
            throw new AccessDeniedHttpException();

        $time = new DateTime('00:00:00');
        $interval = new DateInterval('PT' . $restaurant->getTimeInterval() . 'M');
        $time->add($interval);
        $slotDuration = $time->format('H:i:s');

        return $this->render(
            '@Reservation/Management/view_timeline.html.twig',
            [
                'restaurant' => $id,
                'interval' => $restaurant->getTimeInterval(),
                'minTime' => $restaurant->getEarliestBookingTime()->format('H:i:s'),
                'slotDuration' => $slotDuration,
            ]);
    }
}

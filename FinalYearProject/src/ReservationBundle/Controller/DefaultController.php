<?php

namespace ReservationBundle\Controller;

use AppBundle\Exceptions\RestaurantNotFoundException;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use GuzzleHttp\Exception\GuzzleException;
use Knp\Component\Pager\Paginator;
use ReservationBundle\Entity\ReservationRequest;
use ReservationBundle\Entity\Restaurant;
use ReservationBundle\Form\ReservationRequestType;
use ReservationBundle\Helper\RequestOptions;
use ReservationBundle\Service\SearchApiRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 *
 * @package ReservationBundle\Controller
 */
class DefaultController extends Controller
{
    /** @const string */
    const SEARCH_API_DOMAIN = 'search_api_domain';

    /** @const string */
    const SEARCH_URI = 'search-hybrid';

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $query = $entityManager->getRepository(Restaurant::class)->findAll();

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $restaurants = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)
        );

        return $this->render(
            '@Reservation/Default/index.html.twig',
            [
                'restaurants' => $restaurants
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $searchTerm = $request->get('search');
        $query = $this->getDoctrine()->getRepository(Restaurant::class)
            ->getRestaurantByName($searchTerm);

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $restaurants = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1)
        );

        return $this->render(
            '@Reservation/Default/index.html.twig',
            [
                'restaurants' => $restaurants
            ]
        );
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @return Response
     * @throws RestaurantNotFoundException
     * @throws GuzzleException
     * @throws OptimisticLockException
     * @throws \Exception
     */
    public function requestAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $restaurant = $em->getRepository(Restaurant::class)->find($id);

        if(!$restaurant) {
            throw new RestaurantNotFoundException();
        }

        $reservationRequest = new ReservationRequest();
        $form = $this->createForm(
            ReservationRequestType::class,
            $reservationRequest,
            [
                'action' => $request->getUri(),
                'time_choices' => $restaurant->getTimeChoices()
            ]
        );

        $form->handleRequest($request);
        if($form->isValid()) {
            $reservationRequest->setUser($this->getUser());
            $reservationRequest->setRestaurant($restaurant);
            $reservationRequest->setReservedOn(new DateTime('now'));
            $reservationRequest->setIsSeated();

            $em->persist($reservationRequest);
            $em->flush();

            $day = $form['dateReservedFor']->getData()->format('Y-m-d');
            $optionsService = new RequestOptions($em, $restaurant, $day);
            $options = $optionsService->toArray();

            $searchRequest = new SearchApiRequest();
            $uri = $this->getParameter(self::SEARCH_API_DOMAIN). self::SEARCH_URI;
            $results = $searchRequest->request($uri, $options);

            if(!array_key_exists('error',$results)) {
                $em->getRepository(ReservationRequest::class)->updateReservationRequests($results['parties']);
                $this->addFlash('success', 'Your table has been successfully booked');
            }
            else {
                $formError = new FormError($results['error']);
                $form->addError($formError);
                $reservationRequest->setIsAccepted(false);
                $em->persist($reservationRequest);
                $em->flush();
            }

        }

        return $this->render(
            'ReservationBundle:Default:request.html.twig',
            [
                'form' => $form->createView(),
                'restaurant_name' => $restaurant->getName()
            ]
        );
    }
}

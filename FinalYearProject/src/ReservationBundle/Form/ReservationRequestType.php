<?php

namespace ReservationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ReservationRequestType
 *
 * @package ReservationBundle\Form
 */
class ReservationRequestType extends AbstractType
{    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $timeChoices = $options['time_choices'];
        $builder
            ->add('partySize' , IntegerType::class, [
                'label' => 'No of dinners',
                'attr' => [
                    'min' => 1
                ]
            ])
            ->add('dateReservedFor', DateType::class, [
                'label' => 'Date',
                'format' => DateType::HTML5_FORMAT,
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'js-datepicker'
                ],
                'html5' => false,
            ])
            ->add('timeSlotReservedFor' , ChoiceType::class, [
                'label' => 'Time',
                'choices' => $timeChoices
            ])
            ->add('message', null, [
                'label' => 'Special requirements',
            ])
            ->add('submit', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'ReservationBundle\Entity\ReservationRequest',
            'time_choices' => null,
            'csrf_protection' => false
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'reservationbundle_reservationrequest';
    }


}

<?php

namespace ReservationBundle\Entity;

use DateInterval;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * Restaurant
 *
 * @ORM\Table(name="restaurant")
 * @ORM\Entity(repositoryClass="ReservationBundle\Repository\RestaurantRepository")
 */
class Restaurant
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="restaurants")
     * @ORM\JoinColumn(name="manager", referencedColumnName="id", nullable=false)
     */
    private $manager;

    /**
     * @var int
     *
     * @ORM\Column(name="minDinnerDuration", type="integer", nullable=false)
     */
    private $minDinnerDuration;

    /**
     * @var int
     *
     * @ORM\Column(name="targetCovers", type="integer", nullable=false)
     */
    private $targetCovers;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="earliest_booking_time", type="time", nullable=false)
     */
    private $earliestBookingTime;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="latest_booking_time", type="time", nullable=false)
     */
    private $latestBookingTime;

    /**
     * Time interval between reservation slots
     * 
     * @var int
     *
     * @ORM\Column(name="time_interval", type="integer", nullable=false, options={"default":15})
     */
    private $timeInterval;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="ReservationBundle\Entity\AtomicTable", mappedBy="restaurant")
     */
    protected $tables;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="ReservationBundle\Entity\CompoundTable", mappedBy="restaurant")
     */
    protected $compoundTables;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="ReservationBundle\Entity\ReservationRequest", mappedBy="restaurant")
     */
    protected $reservations;

    /**
     * @var array
     */
    protected $timeChoices = [];

    /**
     * Restaurant constructor.
     */
    public function __construct()
    {
        $this->tables = new ArrayCollection();
        $this->reservations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return User
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @param User $manager
     */
    public function setManager(User $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Set minDinnerDuration
     *
     * @param integer $minDinnerDuration
     *
     * @return Restaurant
     */
    public function setMinDinnerDuration($minDinnerDuration)
    {
        $this->minDinnerDuration = $minDinnerDuration;

        return $this;
    }

    /**
     * Get minDinnerDuration
     *
     * @return int
     */
    public function getMinDinnerDuration()
    {
        return $this->minDinnerDuration;
    }

    /**
     * @return int
     */
    public function getTargetCovers()
    {
        return $this->targetCovers;
    }

    /**
     * @param int $targetCovers
     */
    public function setTargetCovers($targetCovers)
    {
        $this->targetCovers = $targetCovers;
    }

    /**
     * @return DateTime
     */
    public function getEarliestBookingTime()
    {
        return $this->earliestBookingTime;
    }

    /**
     * @param DateTime $earliestBookingTime
     */
    public function setEarliestBookingTime($earliestBookingTime)
    {
        $this->earliestBookingTime = $earliestBookingTime;
    }

    /**
     * @return DateTime
     */
    public function getLatestBookingTime()
    {
        return $this->latestBookingTime;
    }

    /**
     * @param DateTime $latestBookingTime
     */
    public function setLatestBookingTime($latestBookingTime)
    {
        $this->latestBookingTime = $latestBookingTime;
    }

    /**
     * @return int
     */
    public function getTimeInterval()
    {
        return $this->timeInterval;
    }

    /**
     * @param int $timeInterval
     */
    public function setTimeInterval($timeInterval)
    {
        $this->timeInterval = $timeInterval;
    }

    /**
     * Gets the list of tables for the current restaurant
     *
     * @return ArrayCollection
     */
    public function getTables()
    {
        return $this->tables;
    }

    /**
     * Adds a table in the list of table for the current restaurant
     *
     * @param AtomicTable $table
     *
     * @return Restaurant
     */
    public function addTable(AtomicTable $table)
    {
        $this->tables[] = $table;

        return $this;
    }

    /**
     * Removes a table from the list of tables for the current restaurant
     *
     * @param AtomicTable $table
     *
     * @return bool boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTable(AtomicTable $table)
    {
        return $this->tables->removeElement($table);
    }

    /**
     * Gets the list of compound tables for the current restaurant
     *
     * @return ArrayCollection
     */
    public function getCompoundTables()
    {
        return $this->compoundTables;
    }

    /**
     * Adds a compound table in the list of compound tables for the current restaurant
     *
     * @param CompoundTable $compoundTable
     *
     * @return Restaurant
     */
    public function addCompoundTable(CompoundTable $compoundTable)
    {
        $this->compoundTables[] = $compoundTable;

        return $this;
    }

    /**
     * Removes a compound table from the list of compound tables for the current restaurant
     *
     * @param CompoundTable $compoundTable
     *
     * @return bool boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompoundTable(CompoundTable $compoundTable)
    {
        return $this->compoundTables->removeElement($compoundTable);
    }

    /**
     * Gets the list of reservations for the current restaurant
     *
     * @return ArrayCollection
     */
    public function getReservations()
    {
        return $this->reservations;
    }

    /**
     * Adds a reservation request into the list of reservations for the current restaurant
     *
     * @param ReservationRequest $reservation
     *
     * @return Restaurant
     */
    public function addReservation(ReservationRequest $reservation)
    {
        $this->reservations = $reservation;

        return $this;
    }

    /**
     * Removes a reservation request from the list of reservations for the current restaurant
     *
     * @param ReservationRequest $reservation
     *
     * @return bool boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeReservation(ReservationRequest $reservation)
    {
        return $this->reservations->removeElement($reservation);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getTimeChoices()
    {
        $this->getTotalTimeSlots();

        return $this->timeChoices;
    }

    /**
     * calculates the total number of timeSlots available for booking for the current restaurant
     *
     * @return int
     * @throws \Exception
     */
    public function getTotalTimeSlots()
    {
        $totalTimeSlots = 0;
        $startTime = $this->getEarliestBookingTime();
        $time = clone $startTime;

        while($time <= $this->getLatestBookingTime())
        {
            $totalTimeSlots++;
            $this->timeChoices[$time->format('H:i')] = $totalTimeSlots;
            $time->add(
                new DateInterval('PT' . $this->getTimeInterval() . 'M')
            );
        }

        return $totalTimeSlots;
    }
}


<?php

namespace ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CompoundTable
 *
 * @ORM\Table(name="compound_table")
 * @ORM\Entity(repositoryClass="ReservationBundle\Repository\CompoundTableRepository")
 */
class CompoundTable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="capacity", type="integer")
     */
    private $capacity;

    /**
     * @var Restaurant
     * @ORM\ManyToOne(targetEntity="ReservationBundle\Entity\Restaurant", inversedBy="compoundTables")
     * @ORM\JoinColumn(name="restaurant", referencedColumnName="id", nullable=false)
     */
    private $restaurant;

    /**
     * @var AtomicTable
     * @ORM\ManyToOne(targetEntity="ReservationBundle\Entity\AtomicTable")
     * @ORM\JoinColumn(name="atomicTable1", referencedColumnName="id", nullable=false)
     */
    private $atomicTable1;

    /**
     * @var AtomicTable
     * @ORM\ManyToOne(targetEntity="ReservationBundle\Entity\AtomicTable")
     * @ORM\JoinColumn(name="atomicTable2", referencedColumnName="id", nullable=true)
     */
    private $atomicTable2;

    /**
     * @var AtomicTable
     * @ORM\ManyToOne(targetEntity="ReservationBundle\Entity\AtomicTable")
     * @ORM\JoinColumn(name="atomicTable3", referencedColumnName="id", nullable=true)
     */
    private $atomicTable3;

    /**
     * @var AtomicTable
     * @ORM\ManyToOne(targetEntity="ReservationBundle\Entity\AtomicTable")
     * @ORM\JoinColumn(name="atomicTable4", referencedColumnName="id", nullable=true)
     */
    private $atomicTable4;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set capacity
     *
     * @param integer $capacity
     *
     * @return CompoundTable
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;

        return $this;
    }

    /**
     * Get capacity
     *
     * @return int
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * @return Restaurant
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }

    /**
     * @param Restaurant $restaurant
     */
    public function setRestaurant($restaurant)
    {
        $this->restaurant = $restaurant;
    }

    /**
     * @return AtomicTable
     */
    public function getAtomicTable1()
    {
        return $this->atomicTable1;
    }

    /**
     * @param AtomicTable $atomicTable1
     */
    public function setAtomicTable1($atomicTable1)
    {
        $this->atomicTable1 = $atomicTable1;
    }

    /**
     * @return AtomicTable
     */
    public function getAtomicTable2()
    {
        return $this->atomicTable2;
    }

    /**
     * @param AtomicTable $atomicTable2
     */
    public function setAtomicTable2($atomicTable2)
    {
        $this->atomicTable2 = $atomicTable2;
    }

    /**
     * @return AtomicTable
     */
    public function getAtomicTable3()
    {
        return $this->atomicTable3;
    }

    /**
     * @param AtomicTable $atomicTable3
     */
    public function setAtomicTable3($atomicTable3)
    {
        $this->atomicTable3 = $atomicTable3;
    }

    /**
     * @return AtomicTable
     */
    public function getAtomicTable4()
    {
        return $this->atomicTable4;
    }

    /**
     * @param AtomicTable $atomicTable4
     */
    public function setAtomicTable4($atomicTable4)
    {
        $this->atomicTable4 = $atomicTable4;
    }

    /**
     * @return array
     */
    public function getTablesToArray()
    {
        $tables = [];
        array_push($tables, $this->getAtomicTable1()->getId());

        if($this->getAtomicTable2())
            array_push($tables, $this->getAtomicTable2()->getId());
        if($this->getAtomicTable3())
            array_push($tables, $this->getAtomicTable3()->getId());
        if($this->getAtomicTable4())
            array_push($tables, $this->getAtomicTable4()->getId());

        return $tables;
    }
}


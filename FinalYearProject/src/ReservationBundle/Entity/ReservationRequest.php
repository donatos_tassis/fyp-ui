<?php

namespace ReservationBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;
use UserBundle\Entity\User;

/**
 * ReservationRequest
 *
 * @ORM\Table(name="`reservation_request`")
 * @ORM\Entity(repositoryClass="ReservationBundle\Repository\ReservationRequestRepository")
 */
class ReservationRequest
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * The size of the party in people.
     *
     * @var int
     *
     * @ORM\Column(name="partySize", type="integer", nullable=false)
     */
    private $partySize;

    /**
     * The restaurant for which the reservation request is for.
     *
     * @var Restaurant
     * @ORM\ManyToOne(targetEntity="ReservationBundle\Entity\Restaurant", inversedBy="reservations")
     * @ORM\JoinColumn(name="restaurant", referencedColumnName="id", nullable=false)
     */
    private $restaurant;

    /**
     * The User that the reservation is for.
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="reservations")
     * @ORM\JoinColumn(name="user", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * The table(s) that the specific party is going to be seated when it will arrive at the
     * restaurant.
     *
     * @var CompoundTable
     * @ORM\ManyToOne(targetEntity="ReservationBundle\Entity\CompoundTable")
     * @ORM\JoinColumn(name="compoundTable", referencedColumnName="id", nullable=true)
     */
    private $compoundTable;

    /**
     * Boolean flag that indicates if the reservation request is accepted. If it is null it means
     * that the status it has not been decided yet.
     *
     * @var bool
     *
     * @ORM\Column(name="isAccepted", type="boolean", nullable=true)
     */
    private $isAccepted;

    /**
     * Boolean flag that indicates if the reservation has arrived and seated on a table.
     *
     * @var bool
     *
     * @ORM\Column(name="isSeated", type="boolean", nullable=false, options={"default":false})
     */
    private $isSeated;

    /**
     * Message with special customer request.
     *
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;

    /**
     * The date which the reservation request was made on.
     *
     * @var DateTime
     *
     * @ORM\Column(name="reservedOn", type="datetime", nullable=false)
     */
    private $reservedOn;

    /**
     * The date which the reservation request is made for.
     *
     * @var Date
     *
     * @ORM\Column(name="dateReservedFor", type="date", nullable=false)
     */
    private $dateReservedFor;

    /**
     * The date which the reservation request is made for.
     *
     * @var integer
     *
     * @ORM\Column(name="timeSlotReservedFor", type="integer", nullable=false)
     */
    private $timeSlotReservedFor;


    /**
     * ReservationRequest constructor.
     */
    public function __construct()
    {
        $this->reservedOn = new DateTime('now');
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set partySize
     *
     * @param integer $partySize
     *
     * @return ReservationRequest
     */
    public function setPartySize($partySize)
    {
        $this->partySize = $partySize;

        return $this;
    }

    /**
     * Get partySize
     *
     * @return int
     */
    public function getPartySize()
    {
        return $this->partySize;
    }

    /**
     * Set restaurant
     *
     * @param Restaurant $restaurant
     *
     * @return ReservationRequest
     */
    public function setRestaurant(Restaurant $restaurant)
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    /**
     * Get restaurant
     *
     * @return Restaurant
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return ReservationRequest
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set compoundTable
     *
     * @param CompoundTable $compoundTable
     *
     * @return ReservationRequest
     */
    public function setCompoundTable($compoundTable)
    {
        $this->compoundTable = $compoundTable;

        return $this;
    }

    /**
     * Get compoundTable
     *
     * @return CompoundTable
     */
    public function getCompoundTable()
    {
        return $this->compoundTable;
    }

    /**
     * Set isAccepted
     *
     * @param boolean $isAccepted
     *
     * @return ReservationRequest
     */
    public function setIsAccepted($isAccepted)
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }

    /**
     * Get isAccepted
     *
     * @return bool
     */
    public function getIsAccepted()
    {
        return $this->isAccepted;
    }

    /**
     * @return bool
     */
    public function isSeated()
    {
        return $this->isSeated;
    }

    /**
     * @param bool $isSeated
     */
    public function setIsSeated($isSeated = false)
    {
        $this->isSeated = $isSeated;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return ReservationRequest
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set reservedOn
     *
     * @param DateTime $reservedOn
     *
     * @return ReservationRequest
     */
    public function setReservedOn($reservedOn)
    {
        $this->reservedOn = $reservedOn;

        return $this;
    }

    /**
     * Get reservedOn
     *
     * @return DateTime
     */
    public function getReservedOn()
    {
        return $this->reservedOn;
    }

    /**
     * Set reservedFor
     *
     * @param DateTime $dateReservedFor
     *
     * @return ReservationRequest
     */
    public function setDateReservedFor($dateReservedFor)
    {
        $this->dateReservedFor = $dateReservedFor;

        return $this;
    }

    /**
     * Get reservedFor
     *
     * @return Date
     */
    public function getDateReservedFor()
    {
        return $this->dateReservedFor;
    }

    /**
     * @return int
     */
    public function getTimeSlotReservedFor()
    {
        return $this->timeSlotReservedFor;
    }

    /**
     * @param int $timeSlotReservedFor
     */
    public function setTimeSlotReservedFor($timeSlotReservedFor)
    {
        $this->timeSlotReservedFor = $timeSlotReservedFor;
    }
}


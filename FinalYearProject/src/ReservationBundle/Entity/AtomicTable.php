<?php

namespace ReservationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtomicTable
 *
 * @ORM\Table(name="atomic_table")
 * @ORM\Entity(repositoryClass="ReservationBundle\Repository\AtomicTableRepository")
 */
class AtomicTable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Restaurant
     * @ORM\ManyToOne(targetEntity="ReservationBundle\Entity\Restaurant", inversedBy="tables")
     * @ORM\JoinColumn(name="restaurant", referencedColumnName="id", nullable=false)
     */
    private $restaurant;

    /**
     * @var int
     *
     * @ORM\Column(name="capacity", type="integer")
     */
    private $capacity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set restaurant
     *
     * @param Restaurant $restaurant
     *
     * @return AtomicTable
     */
    public function setRestaurant($restaurant)
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    /**
     * Get restaurant
     *
     * @return Restaurant
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }

    /**
     * Set capacity
     *
     * @param integer $capacity
     *
     * @return AtomicTable
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;

        return $this;
    }

    /**
     * Get capacity
     *
     * @return int
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AtomicTable
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}


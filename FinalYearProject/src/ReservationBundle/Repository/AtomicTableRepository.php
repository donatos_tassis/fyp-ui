<?php

namespace ReservationBundle\Repository;

use Doctrine\ORM\EntityRepository;
use ReservationBundle\Entity\AtomicTable;
use ReservationBundle\Entity\Restaurant;

/**
 * AtomicTableRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AtomicTableRepository extends EntityRepository
{
    /** @const int */
    const MIN_CAPACITY = 2;

    /** @const int */
    const MAX_CAPACITY = 8;

    /**
     * @param string $prefix
     * @param int $totalTables
     *
     * @return array
     */
    private function atomicTablesDataProvider($prefix, $totalTables)
    {
        $data = [];

        for($i = 0; $i < $totalTables; $i++) {
            array_push($data, [
                'capacity' => rand(self::MIN_CAPACITY, self::MAX_CAPACITY),
                'name' => $prefix.($i + 1)
            ]);
        }

        return $data;
    }

    /**
     * @param Restaurant $restaurant
     * @param $prefix
     * @param $totalTables
     *
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createAtomicTables(Restaurant $restaurant, $prefix, $totalTables)
    {
        $atomicTables = $this->atomicTablesDataProvider($prefix, $totalTables);
        $atomicTablesList = [];

        foreach($atomicTables as $data) {
            $atomicTable = new AtomicTable();
            $atomicTable->setRestaurant($restaurant);
            $atomicTable->setCapacity($data['capacity']);
            $atomicTable->setName($data['name']);

            $this->getEntityManager()->persist($atomicTable);
            $this->getEntityManager()->flush();

            array_push($atomicTablesList, $atomicTable);
        }

        return $atomicTablesList;
    }

    /**
     * @param $restaurant
     *
     * @return array
     */
    public function getTablesPerRestaurant($restaurant)
    {
        $tables = $this->createQueryBuilder('tab')
            ->where('tab.restaurant = :restaurant')
            ->setParameter('restaurant', $restaurant)
            ->getQuery()
            ->getResult();

        return $tables;
    }
}

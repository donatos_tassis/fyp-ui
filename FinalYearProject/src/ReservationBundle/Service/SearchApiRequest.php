<?php

namespace ReservationBundle\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class SearchApiRequest
 *
 * @package ReservationBundle\Service
 */
class SearchApiRequest
{
    /**
     * @param $uri
     * @param array $options
     * @param string $method
     *
     * @return array
     * @throws GuzzleException
     */
    public function request($uri, array $options, $method = 'POST')
    {
        $client = new Client();

        try {
            $response = $client->request($method, $uri, $options);
            // ToDo: need to handle all errors including no solution found here
            $jsonData = $response->getBody()->getContents();

            return json_decode($jsonData, true);

        } catch (ClientException $error) {
            $message = $error->getMessage();

            if(strpos($message, 'No solution found')) {
                return [
                    'error' => 'No available table found'
                ];
            }

            if(strpos($message, 'Overcrowded party!!')) {
                return [
                    'error' => 'Overcrowded party!! The manager will contact you by email or phone call'
                ];
            }

            if(strpos($message, 'Invalid data passed')) {
                return [
                    'error' => 'Invalid data passed'
                ];
            }
        }
    }
}

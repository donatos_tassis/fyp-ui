<?php

namespace AppBundle\Exceptions;

use Exception;
use Throwable;

class RestaurantNotFoundException extends Exception
{
    /** @const string */
    const RESTAURANT_NOT_FOUND = 'Restaurant not found';

    public function __construct(
        $message = self::RESTAURANT_NOT_FOUND,
        $code = 404,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
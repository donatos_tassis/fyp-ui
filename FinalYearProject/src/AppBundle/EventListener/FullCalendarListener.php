<?php

namespace AppBundle\EventListener;

use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManager;
use InvalidArgumentException;
use ReservationBundle\Entity\ReservationRequest;
use ReservationBundle\Entity\Restaurant;
use ReservationBundle\Helper\SchedulerEvent;
use Toiba\FullCalendarBundle\Event\CalendarEvent;

/**
 * Class FullCalendarListener
 *
 * @package AppBundle\EventListener
 */
class FullCalendarListener
{

    /** @var EntityManager $em */
    private $em;

    /**
     * FullCalendarListener constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param CalendarEvent $calendar
     */
    public function loadEvents(CalendarEvent $calendar)
    {
        $startDate = $calendar->getStart();
        $endDate = $calendar->getEnd();
        $filters = $calendar->getFilters();

        $restaurantId = $filters['restaurant'];
        /** @var Restaurant $restaurant */
        $restaurant = $this->em->getRepository(Restaurant::class)->find($restaurantId);

        $reservations = $this->em->getRepository(ReservationRequest::class)
            ->getAcceptedReservationsPerDayAndRestaurant($restaurantId, $startDate, $endDate);

        $day = $calendar->getStart()->format('Y-m-d');
        $rejectedRequests = $this->em->getRepository(ReservationRequest::class)
            ->getRejectedReservationsPerDayAndRestaurant($restaurant, $day);

        /** @var ReservationRequest $reservation */
        foreach ($reservations as $reservation) {
            $timeSlot = $reservation->getTimeSlotReservedFor();
            $interval = ($timeSlot - 1) * $restaurant->getTimeInterval();
            try {
                $finalInterval = new DateInterval('PT' . $interval . 'M');
            } catch (\Exception $e) {
                throw new InvalidArgumentException($e);
            }

            /** @var DateTime $openingTime */
            $openingTime = $restaurant->getEarliestBookingTime();
            $bookedTime = clone $openingTime;
            $bookedTime->add($finalInterval);
            $bookedTime = $bookedTime->format('H:i:s');
            /** @var DateTime $bookedDate */
            $bookedDate = $reservation->getDateReservedFor();
            $bookedDate = $bookedDate->format('Y-m-d');
            $resourceIds = $reservation->getCompoundTable()->getTablesToArray();

            $calendar->addEvent(new SchedulerEvent(
                $reservation->getId(). '. '.$reservation->getUser()->getSurname(). ' ('.
                $reservation->getPartySize().')',
                $restaurant->getMinDinnerDuration(),
                new DateTime($bookedDate . ' ' .$bookedTime),
                $resourceIds
            ));
        }
    }
}

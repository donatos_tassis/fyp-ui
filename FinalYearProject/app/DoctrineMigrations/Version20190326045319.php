<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Alter foreign key compoundTable into ReservationRequest Entity to be ManyToOne
 * instead of OneToOne
 */
class Version20190326045319 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE reservation_request DROP INDEX UNIQ_5C02341A55CEA1E6,
            ADD INDEX IDX_5C02341A55CEA1E6 (compoundTable)'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE `reservation_request` DROP INDEX IDX_5C02341A55CEA1E6,
            ADD UNIQUE INDEX UNIQ_5C02341A55CEA1E6 (compoundTable)'
        );
    }
}

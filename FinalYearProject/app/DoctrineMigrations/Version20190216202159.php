<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190216202159 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE compound_table (id INT AUTO_INCREMENT NOT NULL, capacity INT NOT NULL,
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE reservation_request ADD CONSTRAINT FK_5C02341A55CEA1E6 
            FOREIGN KEY (compoundTable) REFERENCES compound_table (id)'
        );
        $this->addSql(
            'CREATE UNIQUE INDEX UNIQ_5C02341A55CEA1E6 ON reservation_request (compoundTable)'
        );
        $this->addSql('ALTER TABLE atomic_table ADD compoundTable INT DEFAULT NULL');
        $this->addSql(
            'ALTER TABLE atomic_table ADD CONSTRAINT FK_B330D1E655CEA1E6 
            FOREIGN KEY (compoundTable) REFERENCES compound_table (id)'
        );
        $this->addSql('CREATE INDEX IDX_B330D1E655CEA1E6 ON atomic_table (compoundTable)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE `reservation_request` DROP FOREIGN KEY FK_5C02341A55CEA1E6');
        $this->addSql('ALTER TABLE atomic_table DROP FOREIGN KEY FK_B330D1E655CEA1E6');
        $this->addSql('DROP TABLE compound_table');
        $this->addSql('DROP INDEX IDX_B330D1E655CEA1E6 ON atomic_table');
        $this->addSql('ALTER TABLE atomic_table DROP compoundTable');
        $this->addSql('DROP INDEX UNIQ_5C02341A55CEA1E6 ON `reservation_request`');
    }
}

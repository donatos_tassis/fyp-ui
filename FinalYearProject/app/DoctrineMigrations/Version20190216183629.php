<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190216183629 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE `reservation_request` 
            (id INT AUTO_INCREMENT NOT NULL, user INT NOT NULL, 
            partySize INT NOT NULL, restaurant INT NOT NULL, 
            compoundTable INT DEFAULT NULL, isAccepted TINYINT(1) DEFAULT NULL, 
            message LONGTEXT DEFAULT NULL, reservedOn DATETIME NOT NULL, 
            reservedFor DATETIME NOT NULL, INDEX IDX_5C02341A8D93D649 (user), 
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );

        $this->addSql(
            'ALTER TABLE `reservation_request` ADD CONSTRAINT FK_5C02341A8D93D649 
            FOREIGN KEY (user) REFERENCES `user` (id)');

        $this->addSql('ALTER TABLE user ADD phoneNumber VARCHAR(255) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE `reservation_request`');
        $this->addSql('ALTER TABLE `user` DROP phoneNumber');
    }
}

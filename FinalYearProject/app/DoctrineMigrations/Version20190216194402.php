<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190216194402 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'CREATE TABLE restaurant (id INT AUTO_INCREMENT NOT NULL,
            totalCapacity INT NOT NULL, minDinnerDuration INT NOT NULL, PRIMARY KEY(id))
            DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE atomic_table (id INT AUTO_INCREMENT NOT NULL,
            restaurant INT NOT NULL, capacity INT NOT NULL, name VARCHAR(255) NOT NULL,
            INDEX IDX_B330D1E6EB95123F (restaurant),
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE atomic_table ADD CONSTRAINT FK_B330D1E6EB95123F 
            FOREIGN KEY (restaurant) REFERENCES restaurant (id)'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE atomic_table DROP FOREIGN KEY FK_B330D1E6EB95123F');
        $this->addSql('DROP TABLE restaurant');
        $this->addSql('DROP TABLE atomic_table');
    }
}

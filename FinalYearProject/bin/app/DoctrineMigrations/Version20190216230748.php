<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190216230748 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE restaurant ADD manager INT NOT NULL');
        $this->addSql(
            'ALTER TABLE restaurant ADD CONSTRAINT FK_EB95123FFA2425B9 
            FOREIGN KEY (manager) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_EB95123FFA2425B9 ON restaurant (manager)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE restaurant DROP FOREIGN KEY FK_EB95123FFA2425B9');
        $this->addSql('DROP INDEX IDX_EB95123FFA2425B9 ON restaurant');
        $this->addSql('ALTER TABLE restaurant DROP manager');
    }
}

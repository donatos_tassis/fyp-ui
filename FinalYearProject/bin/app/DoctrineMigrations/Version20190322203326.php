<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190322203326 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE compound_table ADD restaurant INT NOT NULL,
            ADD atomicTable1 INT NOT NULL, ADD atomicTable2 INT DEFAULT NULL,
            ADD atomicTable3 INT DEFAULT NULL, ADD atomicTable4 INT DEFAULT NULL'
        );
        $this->addSql(
            'ALTER TABLE compound_table ADD CONSTRAINT FK_ADC8222AEB95123F FOREIGN KEY (restaurant) 
            REFERENCES restaurant (id)'
        );
        $this->addSql(
            'ALTER TABLE compound_table ADD CONSTRAINT FK_ADC8222AD4189597 FOREIGN KEY (atomicTable1) 
            REFERENCES atomic_table (id)'
        );
        $this->addSql(
            'ALTER TABLE compound_table ADD CONSTRAINT FK_ADC8222A4D11C42D FOREIGN KEY (atomicTable2) 
            REFERENCES atomic_table (id)'
        );
        $this->addSql(
            'ALTER TABLE compound_table ADD CONSTRAINT FK_ADC8222A3A16F4BB FOREIGN KEY (atomicTable3) 
            REFERENCES atomic_table (id)'
        );
        $this->addSql(
            'ALTER TABLE compound_table ADD CONSTRAINT FK_ADC8222AA4726118 FOREIGN KEY (atomicTable4) 
            REFERENCES atomic_table (id)'
        );
        $this->addSql('CREATE INDEX IDX_ADC8222AEB95123F ON compound_table (restaurant)');
        $this->addSql('CREATE INDEX IDX_ADC8222AD4189597 ON compound_table (atomicTable1)');
        $this->addSql('CREATE INDEX IDX_ADC8222A4D11C42D ON compound_table (atomicTable2)');
        $this->addSql('CREATE INDEX IDX_ADC8222A3A16F4BB ON compound_table (atomicTable3)');
        $this->addSql('CREATE INDEX IDX_ADC8222AA4726118 ON compound_table (atomicTable4)');
        $this->addSql('ALTER TABLE atomic_table DROP FOREIGN KEY FK_B330D1E655CEA1E6');
        $this->addSql('DROP INDEX IDX_B330D1E655CEA1E6 ON atomic_table');
        $this->addSql('ALTER TABLE atomic_table DROP compoundTable');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE atomic_table ADD compoundTable INT DEFAULT NULL');
        $this->addSql(
            'ALTER TABLE atomic_table ADD CONSTRAINT FK_B330D1E655CEA1E6 FOREIGN KEY (compoundTable) 
            REFERENCES compound_table (id)'
        );
        $this->addSql('CREATE INDEX IDX_B330D1E655CEA1E6 ON atomic_table (compoundTable)');
        $this->addSql('ALTER TABLE compound_table DROP FOREIGN KEY FK_ADC8222AEB95123F');
        $this->addSql('ALTER TABLE compound_table DROP FOREIGN KEY FK_ADC8222AD4189597');
        $this->addSql('ALTER TABLE compound_table DROP FOREIGN KEY FK_ADC8222A4D11C42D');
        $this->addSql('ALTER TABLE compound_table DROP FOREIGN KEY FK_ADC8222A3A16F4BB');
        $this->addSql('ALTER TABLE compound_table DROP FOREIGN KEY FK_ADC8222AA4726118');
        $this->addSql('DROP INDEX IDX_ADC8222AEB95123F ON compound_table');
        $this->addSql('DROP INDEX IDX_ADC8222AD4189597 ON compound_table');
        $this->addSql('DROP INDEX IDX_ADC8222A4D11C42D ON compound_table');
        $this->addSql('DROP INDEX IDX_ADC8222A3A16F4BB ON compound_table');
        $this->addSql('DROP INDEX IDX_ADC8222AA4726118 ON compound_table');
        $this->addSql(
            'ALTER TABLE compound_table DROP restaurant, DROP atomicTable1, DROP atomicTable2,
            DROP atomicTable3, DROP atomicTable4'
        );
    }
}
